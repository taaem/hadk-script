Before you run the script for your device, you have to tell git who you are.

Here's how to do that:

  git config --global user.name "Replace this text with your full name"

and here's how to add your email adress:

  git config --global user.email "Replace this text with your emailadress"