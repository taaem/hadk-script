#!/usr/bin/bash
cat > $HOME/.hadk.env <<EOF
export MER_ROOT="/home/$USER/mer"
export ANDROID_ROOT="$MER_ROOT/android/droid"
export VENDOR="htc"
export DEVICE="endeavoru"
export PORT_ARCH="armv7hl"
EOF
cat > $HOME/.mersdkubu.profile <<EOF
function hadk() { source $HOME/.hadk.env; echo "Env setup for $DEVICE"; }
export PS1="HABUILD_SDK [\${DEVICE}] $PS1"
hadk
EOF
cat > $HOME/.mersdk.profile <<EOF
function hadk() { source $HOME/.hadk.env; echo "Env. setup for $DEVICE"; }
hadk
EOF
export MER_ROOT=$HOME/mer
TARBALL=mer-i486-latest-sdk-rolling-chroot-armv7hl-sb2.tar.bz2
curl -k -O https://img.merproject.org/images/mer-sdk/$TARBALL
sudo mkdir -p $MER_ROOT/sdks/sdk
cd $MER_ROOT/sdks/sdk
sudo tar --numeric-owner -p -xjf $HOME/$TARBALL
echo "export MER_ROOT=$MER_ROOT" >> ~/.bashrc
echo 'alias sdk=$MER_ROOT/sdks/sdk/mer-sdk-chroot' >> ~/.bashrc
exec bash
echo 'PS1="MerSDK $PS1"' >> ~/.mersdk.profile
cd $HOME
sdk
exit
sdk
sudo zypper ar http://repo.merproject.org/obs/home:/sledge:/mer/latest_i486/ \
curlfix
sudo zypper ar http://repo.merproject.org/obs/nemo:/devel:/hw:/common/sailfish_latest_armv7hl/ \
common 
sudo zypper ref curlfix
sudo zypper dup --from curlfix
sudo zypper ref common
sudo zypper dup --from common
sudo zypper in android-tools createrepo zip
TARBALL=ubuntu-trusty-android-rootfs.tar.bz2
curl -O http://img.merproject.org/images/mer-hybris/ubu/$TARBALL
UBUNTU_CHROOT=$MER_ROOT/sdks/ubuntu
sudo mkdir -p $UBUNTU_CHROOT
sudo tar --numeric-owner -xvjf $TARBALL -C $UBUNTU_CHROOT
ubu-chroot -r $MER_ROOT/sdks/ubuntu
exit
git config --global user.name "Your name here please"
git config --global user.email "Your emailadress here please"
mkdir ~/bin
PATH=~/bin:$PATH
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo
ubu-chroot -r $MER_ROOT/sdks/ubuntu
sudo mkdir -p $ANDROID_ROOT
sudo chown -R $USER $ANDROID_ROOT
cd $ANDROID_ROOT
repo init -u git://github.com/mer-hybris/android.git -b hybris-12.1
mkdir $ANDROID_ROOT/.repo/local_manifests
cd $HOME/.repo/local_manifests/
touch endeavoru.xml
cat > endeavoru.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
<project path="device/sony/scorpion" name="CyanogenMod/android_device_htc_endeavoru" revision="cm-12.1" />
<project path="device/sony/scorpion_windy" name="CyanogenMod/android_device_sony_scorpion_windy" revision="cm-12.1" />
<project path="device/sony/shinano-common" name="CyanogenMod/android_device_sony_shinano-common" revision="cm-12.1" />
<project path="device/sony/msm8974-common" name="CyanogenMod/android_device_sony_msm8974-common" revision="cm-12.1" />
<project path="device/sony/common" name="CyanogenMod/android_device_sony_common" revision="cm-12.1" />
<project path="device/qcom/common" name="CyanogenMod/android_device_qcom_common" revision="cm-12.1" />
<project path="hardware/sony/thermanager" name="CyanogenMod/android_hardware_sony_thermanager" revision="cm-12.1" />
<project path="kernel/sony/msm8974" name="Nokius/android_kernel_sony_msm8974" revision="hybris-12.1" />
<project path="vendor/htc" name="TheMuppets/proprietary_vendor_htc" revision="cm-12.1" />
<project path="rpm/" name="Nokius/droid-hal-scorpion" revision="master" />
<project path="hybris/droid-configs" name="Nokius/droid-config-scorpion" revision="master" />
<project path="hybris/droid-hal-version-endeavoru" name="Nokius/droid-hal-version-endeavoru" revision="master" />
</manifest>
EOF
cd $ANDROID_ROOT
repo sync --fetch-submodules
##Rerun again to make sure that you're fully updated.
repo sync --fetch-submodules
cat > /home/$USER/mer/android/droid/hybris/hybris-boot/fixup-mountpoints << EOF
#!/bin/bash
# Fix up mount points device node names.
# This is broken pending systemd > 191-2 so hack the generated unit files :(
# See: https://bugzilla.redhat.com/show_bug.cgi?id=859297

DEVICE=$1
shift

echo "Fixing mount-points for device $DEVICE"

case "$DEVICE" in
    "mako")
        sed -i \
            -e 's block/platform/msm_sdcc.1/by-name/modem mmcblk0p1 ' \
            -e 's block/platform/msm_sdcc.1/by-name/persist mmcblk0p20 ' \
            -e 's block/platform/msm_sdcc.1/by-name/system mmcblk0p21 ' \
            -e 's block/platform/msm_sdcc.1/by-name/userdata mmcblk0p23 ' \
            "$@"
        ;;

    "grouper")
        sed -i \
            -e 's block/platform/sdhci-tegra.3/by-name/APP mmcblk0p3 ' \
            -e 's block/platform/sdhci-tegra.3/by-name/CAC mmcblk0p4 ' \
            -e 's block/platform/sdhci-tegra.3/by-name/UDA mmcblk0p9 ' \
            -e 's block/platform/sdhci-tegra.3/by-name/MSC mmcblk0p5 ' \
            -e 's block/platform/sdhci-tegra.3/by-name/LNX mmcblk0p2 ' \
            -e 's block/platform/sdhci-tegra.3/by-name/SOS mmcblk0p1 ' \
            "$@"
        ;;

    "tilapia")
        sed -i \
            -e 's block/platform/sdhci-tegra.3/by-name/APP mmcblk0p3 ' \
            -e 's block/platform/sdhci-tegra.3/by-name/CAC mmcblk0p5 ' \
            -e 's block/platform/sdhci-tegra.3/by-name/UDA mmcblk0p10 ' \
            -e 's block/platform/sdhci-tegra.3/by-name/MSC mmcblk0p6 ' \
            -e 's block/platform/sdhci-tegra.3/by-name/LNX mmcblk0p2 ' \
            -e 's block/platform/sdhci-tegra.3/by-name/SOS mmcblk0p1 ' \
            "$@"
        ;;

    "i9305")
	sed -i \
	    -e 's /block/ / ' \
	    "$@"
	;;
	
    "scorpion")
         sed -i \
             -e 's block/platform/msm_sdcc.1/by-name/B2B mmcblk0p25 ' \
             -e 's block/platform/msm_sdcc.1/by-name/DDR mmcblk0p17 ' \
             -e 's block/platform/msm_sdcc.1/by-name/FOTAKernel mmcblk0p16 ' \
             -e 's block/platform/msm_sdcc.1/by-name/LTALabel mmcblk0p18 ' \
             -e 's block/platform/msm_sdcc.1/by-name/TA mmcblk0p1 ' \
             -e 's block/platform/msm_sdcc.1/by-name/aboot mmcblk0p5 ' \
             -e 's block/platform/msm_sdcc.1/by-name/alt_aboot mmcblk0p11 ' \
             -e 's block/platform/msm_sdcc.1/by-name/alt_dbi mmcblk0p10 ' \
             -e 's block/platform/msm_sdcc.1/by-name/alt_rpm mmcblk0p12 ' \
             -e 's block/platform/msm_sdcc.1/by-name/alt_s1sbl mmcblk0p9 ' \
             -e 's block/platform/msm_sdcc.1/by-name/alt_sbl1 mmcblk0p8 ' \
             -e 's block/platform/msm_sdcc.1/by-name/alt_tz mmcblk0p13 ' \
             -e 's block/platform/msm_sdcc.1/by-name/apps_log mmcblk0p22 ' \
             -e 's block/platform/msm_sdcc.1/by-name/boot mmcblk0p14 ' \
             -e 's block/platform/msm_sdcc.1/by-name/cache mmcblk0p24 ' \
             -e 's block/platform/msm_sdcc.1/by-name/dbi mmcblk0p4 ' \
             -e 's block/platform/msm_sdcc.1/by-name/fsg mmcblk0p21 ' \
             -e 's block/platform/msm_sdcc.1/by-name/modemst1 mmcblk0p19 ' \
             -e 's block/platform/msm_sdcc.1/by-name/modemst2 mmcblk0p20 ' \
             -e 's block/platform/msm_sdcc.1/by-name/ramdump mmcblk0p15 ' \
             -e 's block/platform/msm_sdcc.1/by-name/rpm mmcblk0p6 ' \
             -e 's block/platform/msm_sdcc.1/by-name/s1sbl mmcblk0p3 ' \
             -e 's block/platform/msm_sdcc.1/by-name/sbl1 mmcblk0p2 ' \
             -e 's block/platform/msm_sdcc.1/by-name/system mmcblk0p23 ' \
             -e 's block/platform/msm_sdcc.1/by-name/tz mmcblk0p7 ' \
             -e 's block/platform/msm_sdcc.1/by-name/userdata mmcblk0p26 ' \
            "$@" ;;

     *)
        exit 1
        ;;
esac
EOF
source build/envsetup.sh
export USE_CCACHE=1
breakfast $DEVICE
make -j4 hybris-hal
##This will fail, but run this to solve that and rerun make -j4 hybris-hal again and you should be fine.
java -jar /home/$USER/mer/android/droid/out/host/linux-x86/framework/dumpkey.jar build/target/product/security/testkey.x509.pem build/target/product/security/cm.x509.pem build/target/product/security/cm-devkey.x509.pem > /home/$USER/mer/android/droid/out/target/product/$DEVICE/obj/PACKAGING/ota_keys_intermediates/keys
make -j4 hybris-hal
hybris/mer-kernel-check/mer_verify_kernel_config \
./out/target/product/$DEVICE/obj/KERNEL_OBJ/.config
make hybris-boot && make hybris-recovery
exit
SFE_SB2_TARGET=$MER_ROOT/targets/$VENDOR-$DEVICE-$PORT_ARCH
TARBALL_URL=http://releases.sailfishos.org/sdk/latest/targets/targets.json
TARBALL=$(curl $TARBALL_URL | grep "$PORT_ARCH.tar.bz2" | cut -d\" -f4)
curl -O $TARBALL
sudo mkdir -p $SFE_SB2_TARGET
sudo tar --numeric-owner -pxjf $(basename $TARBALL) -C $SFE_SB2_TARGET
sudo chown -R $USER $SFE_SB2_TARGET
cd $SFE_SB2_TARGET
grep :$(id -u): /etc/passwd >> etc/passwd
grep :$(id -g): /etc/group >> etc/group
sb2-init -d -L "--sysroot=/" -C "--sysroot=/" \
-c /usr/bin/qemu-arm-dynamic -m sdk-build \
-n -N -t / $VENDOR-$DEVICE-$PORT_ARCH \
/opt/cross/bin/$PORT_ARCH-meego-linux-gnueabi-gcc
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH -m sdk-install -R rpm --rebuilddb
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH -m sdk-install -R zypper ar \
-G http://repo.merproject.org/releases/mer-tools/rolling/builds/$PORT_ARCH/packages/ \
mer-tools-rolling
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH -m sdk-install -R zypper ref --force
cd $HOME
cat > main.c << EOF
#include <stdlib.h>
#include <stdio.h>
int main(void) {
printf("Hello, world!\n");
return EXIT_SUCCESS;
}
EOF
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH gcc main.c -o test
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH ./test
sudo zypper ref; sudo zypper dup
cd $HOME
sudo mkdir -p $MER_ROOT/devel
sudo chown -R $USER mer/devel
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH -R -m sdk-install ssu ar common http://repo.merproject.org/obs/nemo:/devel:/hw:/common/sailfish_latest_armv7hl/
cd $ANDROID_ROOT
rpm/dhd/helpers/build_packages.sh
##Press Ctrl C to stop this process if you at some point end up with what looks as a freeze and rerun rpm/dhd/helpers/build_packages.sh
mkdir -p tmp
HA_REPO="repo --name=adaptation0-$DEVICE-@RELEASE@"
KS="Jolla-@RELEASE@-$DEVICE-@ARCH@.ks"
sed -e "s|^$HA_REPO.*$|$HA_REPO --baseurl=file://$ANDROID_ROOT/droid-local-repo/$DEVICE|" $ANDROID_ROOT/hybris/droid-configs/installroot/usr/share/kickstarts/$KS > tmp/$KS
RELEASE=2.0.1.11
EXTRA_NAME=-my1
sudo mic create fs --arch $PORT_ARCH \
--debug \
--runtime=native \
--tokenmap=ARCH:$PORT_ARCH,RELEASE:$RELEASE,EXTRA_NAME:$EXTRA_NAME \
--record-pkgs=name,url \
--outdir=sfe-$DEVICE-$RELEASE$EXTRA_NAME \
--pack-to=sfe-$DEVICE-$RELEASE$EXTRA_NAME.tar.bz2 \
$ANDROID_ROOT/tmp/Jolla-@RELEASE@-$DEVICE-@ARCH@.ks
